### 基于若依3.4扩展
1. 集成mybatis-plus插件 3.1.1
2. 生成器修改，并添加参数
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/184426_a90db21a_862.png "屏幕截图.png")
3. 浏览器添加location.hash支持，刷新自动跳转

### 原版在线体验

演示地址：http://ruoyi.vip  

文档地址：http://doc.ruoyi.vip