package com.ruoyi.project.tool.gen.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.utils.text.Convert;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.config.GenConfig;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.tool.gen.domain.TableInfo;
import com.ruoyi.project.tool.gen.service.IGenService;

/**
 * 代码生成 操作处理
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/tool/gen")
public class GenController extends BaseController
{
    private String prefix = "tool/gen";

    @Autowired
    private IGenService genService;

    @Autowired
    private GenConfig genConfig;
    
    @RequiresPermissions("tool:gen:view")
    @GetMapping
    public String gen(ModelMap mmap) {
        mmap.put("author", GenConfig.getAuthor());
        mmap.put("packageName", GenConfig.getPackageName());
        mmap.put("autoRemovePre", GenConfig.getAutoRemovePre());
        mmap.put("tablePrefix", GenConfig.getTablePrefix());
        mmap.put("template", GenConfig.getTemplate());
        mmap.put("outputDir", GenConfig.getOutputDir()); 
        return prefix + "/gen";
    }

	@PostMapping("/apply")
	@ResponseBody
	public AjaxResult apply(String author, String packageName, String autoRemovePre, String tablePrefix, String template,String outputDir) {
		genConfig.setAuthor(author);
		genConfig.setPackageName(packageName);
		genConfig.setAutoRemovePre(autoRemovePre);
		genConfig.setTablePrefix(tablePrefix);
		genConfig.setTemplate(template);
		genConfig.setOutputDir(outputDir);
		return AjaxResult.success();
	}

    @RequiresPermissions("tool:gen:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TableInfo tableInfo)
    {
        startPage();
        List<TableInfo> list = genService.selectTableList(tableInfo);
        return getDataTable(list);
    }

    /**
     * 生成代码
     */
    @RequiresPermissions("tool:gen:code")
    @Log(title = "代码生成", businessType = BusinessType.GENCODE)
    @GetMapping("/genCode/{tableName}")
    @ResponseBody
    public AjaxResult genCode(HttpServletResponse response, @PathVariable("tableName") String tableName) throws IOException
    {
        genService.generatorCode(tableName);
        //genCode(response, data);
        return toAjax(true);
    }

    /**
     * 批量生成代码
     */
    @RequiresPermissions("tool:gen:code")
    @Log(title = "代码生成", businessType = BusinessType.GENCODE)
    @GetMapping("/batchGenCode")
    @ResponseBody
    public AjaxResult batchGenCode(HttpServletResponse response, String tables) throws IOException
    {
        String[] tableNames = Convert.toStrArray(tables);
        genService.generatorCode(tableNames);
        //genCode(response, data);
        return toAjax(true);
    }

    /**
     * 生成zip文件
     * 
     * @param response
     * @param data
     * @throws IOException
     */
    private void genCode(HttpServletResponse response, byte[] data) throws IOException
    {
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"ruoyi.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
    }
}
