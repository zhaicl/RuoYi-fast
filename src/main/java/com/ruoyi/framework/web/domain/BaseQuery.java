package com.ruoyi.framework.web.domain;

import java.util.Map;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 分页工具
 *
 * @author Chill
 */
@Data
@Accessors(chain = true)
@ApiModel(description = "查询条件")
public class BaseQuery {

	/**
	 * 当前页
	 */
	@ApiModelProperty(value = "当前页")
	private Integer pageNum = 1;

	/**
	 * 每页的数量
	 */
	@ApiModelProperty(value = "每页的数量")
	private Integer pageSize = 10;

	/**
	 * 排序的字段名
	 */
	@ApiModelProperty(hidden = true)
	private String orderByColumn;

	/**
	 * 排序方式
	 */
	@ApiModelProperty(hidden = true)
	private String isAsc;

	/** 搜索值 */
	@ApiModelProperty(hidden = true)
	private String searchValue;

	/** 请求参数 */
	@ApiModelProperty(hidden = true)
	private Map<String, Object> params;

}